from flask.ext.login import login_required
from flask import render_template
from pyzabbix import ZabbixAPI
from . import zb
import requests
import logging


logging.basicConfig(level=logging.ERROR)


@zb.route("/graph", methods=["GET"])
@login_required
def rendergraph():
    """
    Polls Zabbix for Asterisk-relevant information and renders it on the Trends
    page
    """
    s = requests.session()
    z = ZabbixAPI("https://zabbix.cvent.net", s)
    z.session.verify = False
    z.session.auth = ("dstanton", "Prs89cam1!CV")
    z.login("dstanton", "Prs89cam1!CV")

    # #-----------------------------
    # Zabbix API implementation Here
    # #-----------------------------
    trigs = z.trigger.get(only_true=1, skipDependent=1, monitored=1, active=1,
                          output="extend",
                          expandDescription=1,
                          expandData="host",
                          withLastEventUnacknowledged=1,)

    unack = z.trigger.get(only_true=1, skipDependent=1, monitored=1, active=1,
                          output="extend",
                          expandDescription=1,
                          expandData="host",)

    unack_ids = [t["triggerid"] for t in unack]
    for t in trigs:
        t["unacknowledged"] = True if t["triggerid"] in unack_ids else False

    for t in trigs:
        if int(t["value"]) == 1:
            priority = ("Host {0} -- {1} {2}"
                        .format(t["host"], t["description"],
                                "(Danger! Unacknowledged Alert)"
                                if t["unacknowledged"]
                                else "Horay! No unacknowledged alerts."))

    return render_template("home/logs.html", priority=priority)


if __name__ == "__main__":
    rendergraph()
