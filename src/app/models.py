from datetime import datetime
from markdown import markdown
import bleach
import hashlib
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flask.ext.login import UserMixin
# TODO replace flask-login with flask-ldap
from . import db, login_manager


class User(UserMixin, db.Model):
    """Define a new user in SQLite"""
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64),
                      nullable=False, unique=True, index=True)
    username = db.Column(db.String(64),
                         nullable=False, unique=True, index=True)
    is_admin = db.Column(db.Boolean)
    password_hash = db.Column(db.String(128))
    avatar_hash = db.Column(db.String(32))
    notes = db.relationship("Note", lazy="dynamic", backref="author")
    numref = db.relationship("Outboundnumber", lazy="dynamic",
                             backref="author")
    zref = db.Column(db.Integer)
    alert_tip = db.Column(db.Integer)

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.email is not None and self.avatar_hash is None:
            self.avatar_hash = hashlib.md5(self.
                                           email.encode("utf-8")).hexdigest()

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def alert_badge(self):
        return Znotify.alert_badge()

    def get_api_token(self, expiration=300):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'user': self.id}).decode('utf-8')

    @staticmethod
    def validate_api_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        id = data.get('user')
        if id:
            return User.query.get(id)
        return None


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Note(db.Model):
    """Annotate something seen in Zabbix / Call Automator Logs"""
    __tablename__ = 'notes'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    body_html = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    author_name = db.Column(db.String(64))
    author_email = db.Column(db.String(64))

    @staticmethod
    def on_changed_body(target, value, oldvalue, initiator):
        allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'code',
                        'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul',
                        'h1', 'h2', 'h3', 'p']
        target.body_html = bleach.linkify(bleach.clean(
            markdown(value, output_format='html'),
            tags=allowed_tags, strip=True))

db.event.listen(Note.body, 'set', Note.on_changed_body)


class Outboundnumber(db.Model):
    """Store a particular phone number for call automation"""
    __tablename__ = "numbers"
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(16))
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))


class Znotify(db.Model):
    """An object to carry Zabbix information and facilitate commenting"""
    __tablename__ = "zobjects"
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    host = db.Column(db.String(64))
    note_id = db.Column(db.Integer, db.ForeignKey('notes.id'))
    userid = db.Column(db.String(64), db.ForeignKey('users.zref'))
