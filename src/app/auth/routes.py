from flask import render_template, redirect, url_for, flash
from flask.ext.login import login_user, logout_user, login_required
from ..models import User
from . import auth
from .forms import LoginForm


@auth.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:
            flash("No account by that name. Please contact the administrator.")
            return redirect(url_for(".login"))
        login_user(user, form.remember_me.data)
        return redirect(url_for("main.dashboard"))
    return render_template("auth/login.html", form=form)


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    flash("You have been successfully logged out.")
    return redirect(url_for("main.exit"))


if __name__ == "__main__":
    login()
    logout()
