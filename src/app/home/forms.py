from flask.ext.wtf import Form
from wtforms import StringField, SubmitField
from wtforms.validators import Required, Length


class DialForm(Form):
    number = StringField("number",
                         validators=[Required(), Length(5, 16)])
    submit = SubmitField("Place Call Now")
