from flask import render_template, flash
from flask.ext.login import login_required
from . import main

import logging
logging.basicConfig(level=logging.DEBUG)


@main.route("/")
def index():
    return render_template("home/index.html")


@main.route("/logs")
@login_required
def logs():
    return render_template("home/logs.html")


@main.route("/dialer", methods=["GET", "POST"])
@login_required
def caller():
    from .forms import DialForm
    f = DialForm()
    if f.validate_on_submit():
        flash("f.validate_on_submit was called")
    else:
        return render_template("home/dialer.html", f=f)


@main.route("/dashboard")
@login_required
def dashboard():
    return render_template("home/dashboard.html")


@main.route("/alerts")
@login_required
def alerts():
    return render_template("home/alerts.html")


@main.route("/logoff")
def exit():
    return render_template("auth/bye.html")


@main.route("/shell")
def shell():
    return render_template("home/shell.html")


if __name__ == "__main__":
    index()
    logs()
    caller()
    dashboard()
    alerts()
    exit()
    shell()
