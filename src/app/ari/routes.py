from flask import render_template
from random import randint as r
from flask.ext.login import login_required
import ari
from . import aster
client = ari.connect("http://127.0.0.1:8088", "dstanton", "arisand")


@aster.route("/dialer", methods=["GET", "POST"])
@login_required
def dialer():
    chan_id = r(1, 999)
    client.channels.originate(endpoint="SIP/damien", extension="1",
                              channelID=chan_id, timeout=-1)


def channel_mon():
    """Displays channels that are reporting active in the Stasis App"""
    current_channels = client.channels.list()
    if len(current_channels) == 0:
        channelwell = "There are no channels connected at this time."
    return render_template("home/dialer.html", channelwell=channelwell)


def stasis_start(channel_obj, event):
    """Handles incoming channel processing"""
    channel = channel_obj.get("channel")
    channelin = "Channel {0} has entered the application"\
        .format(channel.json.get("name"))
    for key, value in channel.json.items():
        print("{0}: {0}".format(key, value))
    return render_template("home/dialer.html", channelin=channelin)


def stasis_end(channel, event):
    """Handles outgoing channel processing"""
    channelout = "Channel {0} has vacated the application"\
        .format(channel.json.get("name"))
    return render_template("home/dialer.html", channelout=channelout)

if __name__ == "__main__":
    client.on_channel_event("StasisStart", stasis_start)
    client.on_channel_event("StasisEnd", stasis_end)
    client.run(apps="xDialer")
