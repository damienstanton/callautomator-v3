from flask import Flask
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager
from flask.ext.moment import Moment
from flask.ext.pagedown import PageDown
from flask.ext.sqlalchemy import SQLAlchemy


db = SQLAlchemy()
moment = Moment()
pagedown = PageDown()
login_manager = LoginManager()


def create_app():
    app = Flask(__name__)
    app.config["DEBUG"] = True  # Load debugger in browser
    app.config.update(SECRET_KEY="7oe<QU)^ym.G%HdfE~s-=TY-T+e2IoZs",
                      SQLALCHEMY_DATABASE_URI="sqlite:///callerdb.db",)
    import logging
    from logging.handlers import SysLogHandler
    syslog_handler = SysLogHandler()
    syslog_handler.setLevel(logging.DEBUG)  # Adjust as needed
    app.logger.addHandler(syslog_handler)
    Bootstrap(app)
    db.init_app(app)
    moment.init_app(app)
    pagedown.init_app(app)
    login_manager.init_app(app)

    from .home import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # DON'T ENABLE THESE UNLESS THEY CAN CONNECT TO SERVER
    from .ari import aster as aster_blueprint
    app.register_blueprint(aster_blueprint)

    from .zabbix import zb as zb_blueprint
    app.register_blueprint(zb_blueprint)

    return app
