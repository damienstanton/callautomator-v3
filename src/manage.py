from app import create_app
from app import db
from app.models import User
from flask.ext.script import Manager

import logging
logging.basicConfig(level=logging.DEBUG)


app = create_app()
manager = Manager(app)


@manager.command
def test():
    from subprocess import call
    call(['nosetests', '-v',
          '--with-coverage', '--cover-package=app', '--cover-branches',
          '--cover-erase', '--cover-html', '--cover-html-dir=cover'])


@manager.command
def adduser(email, username, admin=False):
    """Register a new user in SQLite."""

    from getpass import getpass
    password = getpass()
    password2 = getpass(prompt='Please reenter password to confirm: ')
    if password != password2:
        import sys
        sys.exit('Error: passwords do not match! Try again.')
    user = User(email=email, username=username, password=password,
                is_admin=admin)
    db.create_all()
    db.session.add(user)
    db.session.commit()
    print('User {0} was registered successfully.'.format(username))


@manager.command
def updatedb():
    db.session.commit()
    print("SQLAlchemy session updated successfully")


@manager.command
def flushdb():
    db.session.flush()
    db.create_all()
    print("SQLAlchemy session flushed manually")


if __name__ == '__main__':
    # For Management
    # manager.run()

    # For Deployment
    app.run(host="0.0.0.0", port=81)
