printf $"\n+--------------------------------+\n"
printf $"\n|Call Automator by Damien Stanton|\n"
printf $"\n|Copyright 2014-2015 Cvent, Inc. |\n"
printf $"\n+--------------------------------+\n"
printf $"\n\nInstalling web interface dependencies...\n"
pip install -r requirements.txt
printf $"\n...Done!\n"
# pyzabbix 
printf $"\n\nInstalling Zabbix dependencies...\n"
git clone https://github.com/lukecyca/pyzabbix.git
cd pyzabbix
python3.4 setup.py install
cd ..
printf $"\n...Done!\n"
#ari-py
printf $"\n\nInstalling Asterisk dependencies\n"
git clone https://github.com/asterisk/ari-py.git
cd ari-py
python3.4 setup.py install
cd ..
printf $"\n...Done!\n"
printf $"\nCleaning up...\n"
rm -rf pyzabbix && rm -rf ari-py
printf $"\n+----------------------------------------------------------------------------+\n"
printf $"\n|Setup is complete. Run pip list to check that all dependencies are installed|\n"
printf $"\n+----------------------------------------------------------------------------+\n"
