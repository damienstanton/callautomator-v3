#callautomator-v3
---
An Asterisk call test automator and metrics package

*Requires:* ```Python 3.4``` ```Pip``` ```Asterisk 13``` ```FreePBX 12``` and ```Zabbix 2.0```
##How to install:
```bash
git clone https://github.com/damienstanton/callautomator-v3.git && cd callautomator-v3
./setup.sh
```
Usage
---
If you run these commands with no arguments, you'll be shown how to use them.
##To start the server:
```bash
cd callautomator-v3 && cd src
python manage.py runserver
```
##To manage users:
```bash
cd callautomator-v3 && cd src
python manage.py adduser
# or
python manage.py deleteuser
# or
python manage.py resetpassword
```
##To flush the SQLite DB:
```bash
python manage.py flush
```
##To manually place a call to a known SIP endpoint
*Don't be evil...*
```bash
python manage.py call
```

###Everything else should be done using the GUI which runs by default on port **81**
